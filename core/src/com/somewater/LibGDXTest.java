package com.somewater;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.sql.Time;

public class LibGDXTest extends ApplicationAdapter {
    SpriteBatch batch;
    Texture img;
    boolean touched = false;
    int count = 0;
    boolean playSound = false;
    AssetManager assetManager;

    @Override
    public void create () {
        batch = new SpriteBatch();
        img = new Texture(Gdx.files.internal("badlogic.jpg"));

        assetManager = new AssetManager();
        assetManager.load("fs-damage.wav", Sound.class);
        assetManager.finishLoading();
    }

    @Override
    public void render () {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(img, 50 + count, 50);
        batch.end();

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                System.out.println("postRunnable " + System.currentTimeMillis());
            }
        });
        System.out.println("render " + System.currentTimeMillis());
        if (Gdx.input.isTouched()) {
            if (!touched) {
                System.out.println("Input occurred at x=" + Gdx.input.getX() + ", y=" + Gdx.input.getY());

                touched = true;
                count += 1;

                playSound = true;
            }
        } else {
            touched = false;
        }

        if (playSound) {
            playSound = false;
            if (assetManager.isLoaded("fs-damage.wav")) {
                Sound sound = assetManager.get("fs-damage.wav");
                sound.play(((float) Gdx.input.getX()) / Gdx.graphics.getWidth());
            }
        }
    }
}
