package com.somewater.dropgame;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;

public class GameScreen implements Screen{

    private final DropGame game;

    private Texture dropImage;
    private Texture bucketImage;
    private Sound dropSound;
    private Music rainMusic;

    private OrthographicCamera camera;

    private Rectangle bucket;

    private Array<Rectangle> drops = new Array<Rectangle>(false, 32);
    private long prevDropGenTime = 0;
    private long dropBornTime = 1000;
    private int dropScore = 0;

    public GameScreen(DropGame game) {
        this.game = game;

        dropImage = new Texture(Gdx.files.internal("raindrop/droplet.png"));
        bucketImage = new Texture(Gdx.files.internal("raindrop/bucket.png"));
        dropSound = Gdx.audio.newSound(Gdx.files.internal("raindrop/drop.mp3"));
        rainMusic = Gdx.audio.newMusic(Gdx.files.internal("raindrop/rain.mp3"));

        rainMusic.setLooping(true);
        rainMusic.play();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        bucket = new Rectangle();
        bucket.x = 800 / 2 - 64 / 2;
        bucket.y = 20;
        bucket.width = bucket.height = 64;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        if (Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            bucket.x = touchPos.x - 64 / 2;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            bucket.x -= 200 * delta;
        } else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            bucket.x += 200 * delta;
        }
        if (bucket.x > 800 - 64) bucket.x = 800 - 64;
        if (bucket.x < 0) bucket.x = 0;

        if (TimeUtils.millis() - prevDropGenTime > dropBornTime) {
            prevDropGenTime = TimeUtils.millis();
            createDrop();
        }

        game.batch().setProjectionMatrix(camera.combined);
        game.batch().begin();
        game.font().draw(game.batch(), "Drops: " + this.dropScore, 0, 480);
        game.batch().draw(bucketImage, bucket.x, bucket.y);
        Iterator<Rectangle> dropIt = drops.iterator();
        while (dropIt.hasNext()) {
            Rectangle drop = dropIt.next();
            game.batch().draw(dropImage, drop.x, drop.y);

            drop.y -= 200 * delta;
            if (drop.y + 64 < 0) {
                dropIt.remove();
            } else if (drop.y > 64){
                if (bucket.overlaps(drop)) {
                    dropSound.play();
                    dropIt.remove();
                    dropScore += 1;
                }
            }
        }
        game.batch().end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        dropImage.dispose();
        bucketImage.dispose();
        dropSound.dispose();
        rainMusic.dispose();
    }

    private void createDrop() {
        Rectangle drop = new Rectangle(MathUtils.random(800 - 64), 480, 64, 64);
        drops.add(drop);

        dropBornTime = Math.max(300, dropBornTime - 10);
    }
}
