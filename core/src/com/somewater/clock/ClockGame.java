package com.somewater.clock;

import com.badlogic.gdx.Game;

public class ClockGame extends Game{
    @Override
    public void create() {
        setScreen(new ClockScreen());
    }
}
