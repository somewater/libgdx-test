package com.somewater.clock;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.TimeUtils;

public class ClockScreen implements Screen {

    OrthographicCamera camera;

    Texture clockImg;
    Texture secImg;
    Texture minImg;
    Texture hourImg;

    float imgToSizeX = 1;
    float imgToSizeY = 1;
    float width = 500;
    float height = 500;

    SpriteBatch batch;

    public ClockScreen() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        clockImg = new Texture(Gdx.files.internal("clock/clock.png"));
        secImg = new Texture(Gdx.files.internal("clock/sec.png"));
        minImg = new Texture(Gdx.files.internal("clock/min.png"));
        hourImg = new Texture(Gdx.files.internal("clock/hour.png"));

        batch = new SpriteBatch();

        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
            batch.draw(clockImg, 0, 0, 500, 500);
            batch.draw( hourImg, width * 0.5f, height * 0.5f,
                        0f, 0f,
                        hourImg.getWidth() * imgToSizeX, hourImg.getHeight() * imgToSizeY,
                        1.0f, 1.0f,
                        TimeUtils.millis() * 0.00001f,
                        0, 0, hourImg.getWidth(), hourImg.getHeight(), false, false
                    );
            batch.draw(secImg, 0, 0, secImg.getWidth() * imgToSizeX, secImg.getHeight() * imgToSizeY);
            batch.draw(minImg, 0, 0, minImg.getWidth() * imgToSizeX, minImg.getHeight() * imgToSizeY);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
        imgToSizeX = (float)width / clockImg.getWidth();
        imgToSizeY = (float)height / clockImg.getHeight();
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
