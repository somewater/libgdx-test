package com.somewater.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.somewater.clock.ClockGame;
import com.somewater.dropgame.DropGame;

public class DesktopClockTest {
    public static void main (String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Clock";
        config.width = 500;
        config.height = 500;
        new LwjglApplication(new ClockGame(), config);
    }
}
