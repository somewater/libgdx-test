package com.somewater.lwjgl;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import static org.lwjgl.opengl.GL11.*;

public class LwBase {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    private boolean running = true;
    protected float spin = 0f;

    public static void main(String[] args) throws LWJGLException {
        new LwBase().start();
    }

    protected void start() throws LWJGLException {
        Display.setTitle("Lw Test");
        Display.setResizable(true);
        Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
        Display.setVSyncEnabled(true);
        Display.setFullscreen(false);

        Display.create();

        create();

        resize();

        while(running && !Display.isCloseRequested()) {
            if (Display.wasResized())
                resize();

            render();
            Display.update();

            Display.sync(30);

            keys();
        }

        dispose();
        Display.destroy();
    }

    public void exit() {
        running = false;
    }

    protected void create() {
        glDisable(GL_DEPTH_TEST);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glShadeModel(GL_FLAT);

        glClearColor(0f, 0f, 0f, 0f);
        glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
    }

    protected void render() {
        throw new NotImplementedException();
    }

    protected void resize() {
        glViewport(0, 0, Display.getWidth(), Display.getHeight());
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-20.0,20.0,-20.0,20.0,100,-100);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
    }

    protected void dispose() {
    }

    private void keys() {
        if (Keyboard.isKeyDown(Keyboard.KEY_LEFT))
            spin -= 1f;
        else if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT))
            spin += 1f;
    }
}
