package com.somewater.lwjgl;

import org.lwjgl.LWJGLException;

import static org.lwjgl.opengl.GL11.*;

/**
 * Нарисовать окружность
 */
public class LwCircle extends LwBase {
    @Override
    protected void render() {
        glClear(GL_COLOR_BUFFER_BIT);

        int circle_points=30;

        glCullFace(GL_FRONT); // отменить рисование front граней
        glEnable(GL_CULL_FACE);

        glFrontFace(GL_CW);// front-грани теперь по часовой стрелки, т.е. ниже мы рисуем back-грань
        glPolygonMode(GL_FRONT,GL_POINT);
        glPolygonMode(GL_BACK,GL_LINE);
        glBegin(GL_POLYGON);

        for(int i=0;i<circle_points;i++)
        {
            double angle=2*Math.PI*i/circle_points;
            glVertex2d(Math.cos(angle)*10,Math.sin(angle)*10);
        }
        glEnd();
    }

    public static void main(String[] args) throws LWJGLException {
        new LwCircle().start();
    }
}
