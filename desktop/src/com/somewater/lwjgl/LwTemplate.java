package com.somewater.lwjgl;

import org.lwjgl.LWJGLException;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 */
public class LwTemplate extends LwBase {
    @Override
    protected void render() {
        glClear(GL_COLOR_BUFFER_BIT);
        super.render();
    }

    public static void main(String[] args) throws LWJGLException {
        new LwTemplate().start();
    }
}
