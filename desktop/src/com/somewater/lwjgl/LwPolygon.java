package com.somewater.lwjgl;

import org.lwjgl.LWJGLException;

import static org.lwjgl.opengl.GL11.*;

/**
 * Нарисовать полигон
 */
public class LwPolygon extends LwBase {
    @Override
    protected void render() {
        glColor3f(1.0f, 1.0f, 1.0f);
        glBegin(GL_POLYGON);
        glVertex3f(0.25f, 0.25f, 0.0f);
        glVertex3f(0.75f, 0.25f, 0.0f);
        glVertex3f(0.75f, 0.75f, 0.0f);
        glVertex3f(0.25f, 0.75f, 0.0f);
        glEnd();
        glFlush();
    }

    public static void main(String[] args) throws LWJGLException {
        new LwPolygon().start();
    }
}
