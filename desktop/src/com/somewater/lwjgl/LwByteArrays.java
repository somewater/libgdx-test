package com.somewater.lwjgl;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.MemoryUtil;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;

/**
 *
 */
public class LwByteArrays extends LwBase {

    private IntBuffer verticesBuf;
    private DoubleBuffer interwBuf;
    private FloatBuffer colorsBuf;

    @Override
    protected void create() {
        super.create();

        int[] vertices= new int[]{
                2, 2,
                10, 10,
                10, 5,
                5, -5,
                10, -10,
                5, 0};
        float[] colors= new float[]{
                1.0f, 0f, 1f,
                200f, 1f, 1f,
                1f, 1.0f, 0f,
                0.75f, 0.75f, 0.75f,
                0.5f, 0.1f, 0.5f,
                0.5f, 0.5f, 0.5f};
        double[] interwined = new double[]{
                1.0, 0.2, 1.0, 10.0, 10.0, 0.0,
                1.0, 0.2, 0.2, 0.0,  20.0, 0.0,
                1.0, 1.0, 0.2, 10.0, 30.0, 0.0,
                0.2, 1.0, 0.2, 20.0, 30.0, 0.0,
                0.2, 1.0, 1.0, 30.0, 20.0, 0.0,
                0.2, 0.2, 1.0, 20.0, 10.0, 0.0};

        colorsBuf = BufferUtils.createFloatBuffer(colors.length).put(colors);
        colorsBuf.flip();


        verticesBuf = BufferUtils.createIntBuffer(vertices.length).put(vertices);
        verticesBuf.flip();


        interwBuf = ByteBuffer.allocateDirect(interwined.length * 8).asDoubleBuffer().put(interwined);
        interwBuf.flip();
    }

    @Override
    protected void render() {
        glClear(GL_COLOR_BUFFER_BIT);

        glEnableClientState(GL_COLOR_ARRAY);
        glEnableClientState(GL_VERTEX_ARRAY);

        glColorPointer(3, 0, colorsBuf);
        glVertexPointer(2, 0, verticesBuf);
        //glColorPointer(3,GL_FLOAT, 6 * 4, interwBuf);
        //glVertexPointer(3,GL_FLOAT, 6 * 4, interwBuf);

        glPolygonMode(GL_FRONT, GL_FILL);
        glBegin(GL_TRIANGLES);
            /*glVertex2f(vertices[0 * 2], vertices[0 * 2 + 1]);
            glColor3f(colors[0 * 2 + 0], colors[0 * 2 + 1], colors[0 * 2 + 2]);
            glVertex2f(vertices[1 * 2], vertices[1 * 2 + 1]);
            glColor3f(colors[1 * 2 + 0], colors[1 * 2 + 1], colors[1 * 2 + 2]);
            glVertex2f(vertices[2 * 2], vertices[2 * 2 + 1]);
            glColor3f(colors[2 * 2 + 0], colors[2 * 2 + 1], colors[2 * 2 + 2]);*/

            glArrayElement(0);
            glArrayElement(3);
            glArrayElement(5);
        glEnd();
        glFlush();
    }

    public static void main(String[] args) throws LWJGLException {
        new LwByteArrays().start();
    }
}
